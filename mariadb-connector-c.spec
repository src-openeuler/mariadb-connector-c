Name:           mariadb-connector-c
Version:        3.4.3
Release:        1
Summary:        MariaDB connector library in C
License:        LGPL-2.1-or-later
URL:            https://mariadb.com/kb/en/mariadb-connector-c/
Source0:        https://archive.mariadb.org/connector-c-%{version}/%{name}-%{version}-src.tar.gz

Patch0003:      mariadb-connector-c-3.1.13-src-sw.patch
Patch0004:      add-riscv64-to-lib64-matches.patch

BuildRequires:  libcurl-devel zlib-devel openssl-devel
BuildRequires:  cmake gcc
Provides:       %{name}-config%{?_isa} %{name}-config
Obsoletes:      %{name}-config

%description
This package is used for connecting C/C++ programs to MariaDB and
MySQL database.


%package devel
Summary:        Mariadb-connector-c library and header files
Requires:       %{name} = %{version}-%{release}
Requires:       openssl-devel

%description devel
This package includes library and header files for development.

%prep
%autosetup -n %{name}-%{version}-src -p1

%build
%cmake \
       -DCMAKE_BUILD_TYPE=RelWithDebInfo \
       -DCMAKE_SYSTEM_PROCESSOR="%{_arch}" \
       -DCMAKE_COMPILE_WARNING_AS_ERROR=0 \
       -DMARIADB_UNIX_ADDR=%{_sharedstatedir}/mysql/mysql.sock \
       -DMARIADB_PORT=3306 \
       -DWITH_EXTERNAL_ZLIB=YES \
       -DWITH_SSL=OPENSSL \
       -DWITH_MYSQLCOMPAT=ON \
       -DINSTALL_LAYOUT=RPM \
       -DCMAKE_INSTALL_PREFIX="%{_prefix}" \
       -DINSTALL_BINDIR="bin" \
       -DINSTALL_LIBDIR="%{_lib}" \
       -DINSTALL_INCLUDEDIR="include/mysql" \
       -DINSTALL_PLUGINDIR="%{_lib}/mariadb/plugin" \
       -DINSTALL_PCDIR_RPM="%{_lib}/pkgconfig" \
       -DWITH_UNITTEST=ON
%cmake_build

%install
%cmake_install
%delete_la_and_a

ln -s mariadb_config %{buildroot}%{_bindir}/mysql_config
ln -s mariadb_version.h %{buildroot}%{_includedir}/mysql/mysql_version.h
mkdir -p %{buildroot}%{_sysconfdir}/
mkdir -p %{buildroot}%{_sysconfdir}/my.cnf.d

%check
%{buildroot}%{_bindir}/mariadb_config
%ctest

%pretrans -p <lua>
path = "%{_libdir}/mariadb"
st = posix.stat(path)
if st and st.type == "link" then
  os.remove(path)
end
path = "%{_libdir}/mysql"
st = posix.stat(path)
if st and st.type == "link" then
  os.remove(path)
end

%files
%doc README
%license COPYING.LIB
%dir %{_sysconfdir}/my.cnf.d
%dir %{_libdir}/mariadb
%{_libdir}/libmariadb.so.*
%dir %{_libdir}/mariadb/plugin
%{_libdir}/mariadb/plugin/*

%files devel
%{_bindir}/mariadb_config
%{_bindir}/mysql_config
%dir %{_includedir}/mysql
%{_includedir}/mysql/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/libmariadb.pc
%{_mandir}/man3/{mariadb,mysql}_*.3*

%changelog
* Sun Nov 24 2024 Funda Wang <fundawang@yeah.net> - 3.4.3-1
- update to 3.4.3

* Mon Jun 03 2024 laokz <zhangkai@iscas.ac.cn> - 3.3.8-2
- add riscv64 to lib64 matches

* Tue Feb 06 2024 Jiangtian Feng <fengjiangtian@huawei.com> - 3.3.8-1
- update package to 3.3.8

* Fri Jun 16 2023 dillon chen <dillon.chen@gmail.com> - 3.3.5-1
- update package to 3.3.5

* Thu Jun 15 2023 dillon chen <dillon.chen@gmail.com> - 3.2.7-1
- update package to 3.2.7

* Fri Jun  9 2023 dillon chen <dillon.chen@gmail.com> - 3.1.21-1
- update package to 3.1.21

* Thu Mar 30 2023 laokz <zhangkai@iscas.ac.cn> - 3.1.13-7
- Fix riscv64 pkgconfig directory

* Tue Feb 07 2023 peijiankang<peijiankang@kylinos.cn> - 3.1.13-6
- Revert the compilation dependency of compat-openssl to openssl 

* Mon Feb 06 2023 peijiankang<peijiankang@kylinos.cn> - 3.1.13-5
- Change the compilation dependency of openssl to compat-openssl 

* Wed Nov 23 2022 wuzx<wuzx1226@qq.com> - 3.1.13-4
- Add sw64 architecture

* Mon Nov 14 2022 zhaozhen <zhaozhen@loongson.cn> - 3.1.13-3
- Add loongarch support

* Mon Oct 10 2022 Jiangtian Feng <fengjiangtian@huawei.com> -3.1.13-2
- Type:CVE
- CVE:CVE-2022-37434
- SUG:NA
- DESC: fix CVE-2022-37434
- fix CVE-2022-37434

* Thu Dec 9 2021 yanglongkang <yanglongkang@huawei.com> - 3.1.13-1
- update package to 3.1.13

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 3.0.6-9
- DESC: delete -S git from %autosetup, and delete BuildRequires git

* Tue Jun 29 2021 zhouwenpei <zhouwenpei1@huawei.com> - 3.0.6-8
- add buildrequire gcc

* Tue Nov 10 2020 yanglongkang <yanglongkang@huawei.com> - 3.0.6-7
- Temporarily roll back the version 3.0.6 to resolve the conflict

* Tue Jul 21 2020 jixinjie <jixinjie@huawei.com> - 3.1.9-1
- update package to 3.1.9

* Mon Jan 13 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.0.6-6
- Type:enhance
- ID:NA
- SUG:restart
- DESC:repackaged

* Wed Jan 8 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.0.6-5
- Type:enhance
- ID:NA
- SUG:restart
- DESC:repackaged

* Sun Dec 29 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.0.6-4
- Type:bugfix
- ID:NA
- SUG:restart
- DESC:add the needed dir

* Wed Jul 18 2018 openEuler Buildteam <buildteam@openeuler.org> - 3.0.6-3
- Package init

